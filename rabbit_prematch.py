import time
from logging import getLogger
from typing import Dict, Optional, NoReturn, List, Tuple

import pika
import ujson
from credentials import LSPORTusernamePreMatch, LSPORTpackageIdPreMatch, LSPORTguidPreMatch, LSPORTpasswordPreMatch
from pika.exceptions import ProbableAuthenticationError
from tornado.escape import json_decode
from tornado.httpclient import HTTPClient
from tornado.httpclient import HTTPClientError

import settings
from utils import RedisPoll, get_request, update_obj, process_bets_changes, set_bets_prices

logger = getLogger(__name__)

PREMATCH_RIGHT_STATUS = 1  # event Not started, all others are wrong
EVENT_FINISHED_STATUS = 3
LSPORT_PREMATCH_URL = 'http://prematch.lsports.eu/OddService/'
LOGIN_PREMATCH = f"""?username={LSPORTusernamePreMatch}&password={LSPORTpasswordPreMatch}&guid={LSPORTguidPreMatch}"""

ENABLE_PREMATCH_URL = "http://prematch.lsports.eu/OddService/EnablePackage"
FIRST_LANGUAGE = settings.LSPORT_LANGUAGES[0]
LANGUAGE_IN_RABBIT = 'en'
GET_EVENTS_URL = "".join([LSPORT_PREMATCH_URL, "GetEvents", LOGIN_PREMATCH, "&bookmakers=", str(settings.LSPORT_ONE_PROVIDER_ID)])
GET_SCORES_URL = "".join([LSPORT_PREMATCH_URL, "GetScores", LOGIN_PREMATCH])
DEFAULTS_MARKETS = (1, 2, 7, 13, 52)

# Structures in Redis:
# events_statuses = {fixture_id: status}
# prematch_fixtures_ids = {fixture_id: (sport_id, location_id)} - events are here:
# prematch_spo_{sport_id}_loc_{location_id}_lang_{lang} = [events]
# bets_prices_<fixture id> = {<market id>: {<bet id>: {"coef": <bet Price>, "status": <bet status>}}}
# Bet statuses:
# 1 - bets can be placed
# 2 - (Suspended) bets cannot be placed
# 3 - (got result) bets cannot be placed


def get_events(message: bytes) -> (Optional[List], Optional[int]):
    """Get events from rabbit message"""

    timestamp = None
    if message:
        message = message.decode()
        message = ujson.loads(message)
        header = message.get("Header")
        if header:
            timestamp = header.get("ServerTimestamp")
        body = message.get("Body")
        if body and body.get("Events"):
            return body["Events"], timestamp
    return None, timestamp


def change_events_in_redis(r: RedisPoll, sport_location_ids: List[Tuple], events_to_change: Dict,
                           events_to_delete: Dict, lang: str) -> NoReturn:
    """
        1) Changing events in redis
        2) Deleting events in redis
    """

    for s_l_id in sport_location_ids:
        sport_id, location_id = s_l_id
        events_changes = events_to_change.get(s_l_id)
        delete_events = events_to_delete.get(s_l_id)
        events_before = None
        filtered_events = []
        if events_changes:  # 1)Changing events in reids
            events_before = r.get(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}')
            if events_before:
                events_before = ujson.loads(events_before)
                update_obj(events_before, events_changes)
            else:
                for event in events_changes:
                    if event.get("Markets"):
                        filtered_events.append(event)
                if filtered_events:
                    r.set(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}',
                          ujson.dumps(filtered_events).encode('utf8'))
        if delete_events:  # 2) Deleting events in redis
            if not filtered_events:
                if not events_before:
                    events_before = r.get(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}')
                    if events_before:
                        events_before = ujson.loads(events_before)
                if events_before:
                    filtered_events = []
                    for event in events_before:
                        if event.get("FixtureId") not in delete_events:
                            filtered_events.append(event)
                    if filtered_events:
                        r.set(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}',
                              ujson.dumps(filtered_events).encode('utf8'))
                    else:
                        r.delete(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}')
        elif events_before:
            r.set(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}',
                  ujson.dumps(events_before).encode('utf8'))


def update_events(changes: List[Dict], r: RedisPoll, timestamp_rmq) -> NoReturn:
    """
        Updating redis data with changes from rabbit:
        1) Changing and deleting events in redis
        2) Changing bet statuses
    """

    events_statuses = {}
    fixture_ids = []
    filtered_events = []
    for event in changes:  # Getting FixtureIds
        fixture_id = event.get("FixtureId")
        if fixture_id:
            fixture_ids.append(fixture_id)
            filtered_events.append(event)
    if not fixture_ids:
        return
    changes = filtered_events
    prematch_fixtures_ids = r.get('prematch_fixtures_ids')  # Previous FixtureIds
    if not prematch_fixtures_ids:
        return
    prematch_fixtures_ids = ujson.loads(prematch_fixtures_ids)
    sports_locations_ids = []
    events_to_delete, events_to_change = {}, {}
    fixture_bets_prices = {}
    for event in changes:  # Processing all changes:
        fixture = event.get('Fixture')
        markets = event.get("Markets")
        fixture_id = str(event["FixtureId"])

        set_bets_prices(markets, fixture_bets_prices, fixture_id)
        if fixture:
            if fixture.get("Status") and fixture["Status"] != PREMATCH_RIGHT_STATUS:
                continue  # skip not future events
        s_l_id = prematch_fixtures_ids.get(fixture_id)
        if not s_l_id:
            if fixture:
                s_l_id = (fixture["Sport"]["Id"], fixture["Location"]["Id"])
            else:  # skip event without fixture
                continue
        else:
            s_l_id = tuple(s_l_id)
        sports_locations_ids.append(s_l_id)
        to_delete = False
        if fixture:
            if fixture["Status"] == PREMATCH_RIGHT_STATUS:
                if s_l_id in events_to_change:
                    events_to_change[s_l_id].append(event)
                else:
                    events_to_change[s_l_id] = [event]
            else:  # Selecting events to delete if event status not actual
                to_delete = True
                if s_l_id in events_to_delete:
                    events_to_delete[s_l_id].append(fixture_id)
                else:
                    events_to_delete[s_l_id] = [fixture_id]
                events_statuses = {fixture_id: fixture["Status"]}
        if markets and not to_delete:
            for i in range(len(markets) - 1, -1, -1):
                market_id = markets[i]["Id"]
                if market_id not in DEFAULTS_MARKETS:  # Filtering markets
                    markets.pop(i)
        if not fixture:  # Selecting events for update:
            need_change = False
            for key, value in event.items():
                if key != "FixtureId" and value:
                    need_change = True
            if need_change:
                if s_l_id in events_to_change:
                    events_to_change[s_l_id].append(event)
                else:
                    events_to_change[s_l_id] = [event]
    langs = settings.LSPORT_LANGUAGES
    #  write_changes_to_redis:
    if fixture_ids and LANGUAGE_IN_RABBIT in langs:  # 1) Changing and deleting events in redis:
        change_events_in_redis(r, sports_locations_ids, events_to_change, events_to_delete,
                               LANGUAGE_IN_RABBIT)
        langs = langs.copy()
        langs.remove(LANGUAGE_IN_RABBIT)

    if fixture_ids:
        for lang in langs:  # For other languages - 1) Changing and deleting events in redis:
            change_events_in_redis(r, sports_locations_ids, events_to_change, events_to_delete,
                                   lang)

    process_bets_changes(fixture_bets_prices, "prematch", logger)

    if events_statuses:  # 2) Changing bet statuses:
        old_events_statuses = r.get('events_statuses')
        if not old_events_statuses:
            r.set('events_statuses', ujson.dumps(events_statuses).encode('utf8'))
        else:
            change = False
            old_events_statuses = ujson.loads(old_events_statuses)
            for fixture_id, status in events_statuses.items():
                previous_status = old_events_statuses.get(fixture_id)
                if not previous_status or previous_status != status:
                    change = True
                    old_events_statuses[fixture_id] = status
            if change:
                r.set('events_statuses', ujson.dumps(old_events_statuses).encode('utf8'))


def callback(ch, method, properties, body: bytes) -> NoReturn:
    """Callback for RabbitMQ message. Updating data in redis with message."""

    logger.info(f" [x] Received {body}")
    events, timestamp = get_events(body)
    r = RedisPoll.get_instance()
    if events:
        try:
            update_events(events, r, timestamp)
        except Exception as e:
            logger.error(e, exc_info=True)
    try:
        last_timestamp = r.get('prematch_last_timestamp')
        last_timestamp = ujson.loads(last_timestamp) if last_timestamp else None
        if timestamp:
            if not last_timestamp or (last_timestamp and timestamp > last_timestamp):
                r.set('prematch_last_timestamp', timestamp)
    except Exception as e:
        logger.error(e, exc_info=True)


def write_first_prematch_to_redis() -> NoReturn:
    """
        Getting snapshots of events with different languages and writing data to redis:
        1) Grouping events by sport and location and writing to redis
        2) Writing all sports, locations, events ids, statuses and bet prices to redis
    """

    r = RedisPoll.get_instance()
    http_client = HTTPClient()
    fixture_bets_prices = {}
    timestamp = prematch_fixtures_ids = None
    for lang in settings.LSPORT_LANGUAGES:
        prematch_fixtures_ids = {}
        try:  # Getting snapshot
            response = http_client.fetch(get_request(GET_EVENTS_URL + "&Lang=" + lang))
        except HTTPClientError:
            # Try to reenable package:
            HTTPClient().fetch(get_request("".join([ENABLE_PREMATCH_URL, LOGIN_PREMATCH])))
            time.sleep(3)
            # Another try to get snapshot:
            response = http_client.fetch(get_request(GET_EVENTS_URL + "&Lang=" + lang))
        data = json_decode(response.body)
        events = data["Body"]
        if not timestamp:
            timestamp = data['Header']['ServerTimestamp']
        sports_dict = dict()
        events_dict = {}
        for event in events:  # Processing all events
            fixture = event.get('Fixture')
            markets = event.get('Markets')
            fixture_id = event.get("FixtureId")
            if not fixture_id:
                continue
            if markets and fixture_bets_prices is not None:
                fixture_bets_prices[fixture_id] = {}
                for i in range(len(markets) - 1, -1, -1):
                    market_id = markets[i]["Id"]
                    fixture_bets_prices[fixture_id][market_id] = {}
                    for provider in markets[i]["Providers"]:
                        if provider.get("Bets"):
                            for bet in provider["Bets"]:
                                info = {"coef": bet["Price"], "status": bet["Status"]}
                                if bet.get("Settlement"):
                                    info["result"] = bet["Settlement"]
                                fixture_bets_prices[fixture_id][market_id][bet["Id"]] = info
            if fixture:
                status = fixture.get("Status")
                if status and status != PREMATCH_RIGHT_STATUS:
                    continue
                location_id = fixture["Location"]["Id"]
                sport_id = fixture["Sport"]["Id"]
                sport_name = fixture["Sport"]["Name"]

                if markets:
                    location_name = fixture["Location"]["Name"]
                    s_l_id = (sport_id, location_id)
                    prematch_fixtures_ids[fixture_id] = s_l_id
                    for i in range(len(markets) - 1, -1, -1):
                        market_id = markets[i]["Id"]
                        if market_id not in DEFAULTS_MARKETS:  # Filtering markets
                            markets.pop(i)
                    if sport_id not in sports_dict:
                        sports_dict[sport_id] = {"Id": sport_id, "Name": sport_name, "Locations": {
                            location_id: {"Id": location_id, "Name": location_name, "EventsSum": 1}}}
                    else:
                        if location_id not in sports_dict[sport_id]["Locations"]:
                            sports_dict[sport_id]["Locations"][location_id] = {"Id": location_id, "Name": location_name,
                                                                               "EventsSum": 1}
                        else:
                            sports_dict[sport_id]["Locations"][location_id]["EventsSum"] += 1

                    if s_l_id not in events_dict:
                        events_dict[s_l_id] = [event]
                    else:
                        events_dict[s_l_id].append(event)

        for s_l_id, events_ in events_dict.items():  # Writing grouped events to redis
            sport_id, location_id = s_l_id
            r.set(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}',
                  ujson.dumps(events_).encode('utf8'))

        sports_locations_list = []
        for sport in sports_dict.values():
            sport["Locations"] = list(sport["Locations"].values())
            sports_locations_list.append(sport)
        if sports_locations_list:  # Writing all sports and locations
            r.set(f'prematch_sports_locations_{lang}',
                  ujson.dumps(sports_locations_list).encode('utf8'))
        if fixture_bets_prices:  # Writing bets changes to redis:
            process_bets_changes(fixture_bets_prices, "prematch", logger)
            fixture_bets_prices = None
    if prematch_fixtures_ids:  # Writing ids of events and events statuses
        r.set('prematch_fixtures_ids',
              ujson.dumps(prematch_fixtures_ids).encode('utf8'))
        events_statuses = {fixt_id: PREMATCH_RIGHT_STATUS for fixt_id in prematch_fixtures_ids.keys()}
        r.set('events_statuses',
              ujson.dumps(events_statuses).encode('utf8'))


class LsportRabbitPrematch:
    def __init__(self):
        credentials = pika.PlainCredentials(username=LSPORTusernamePreMatch, password=LSPORTpasswordPreMatch)
        try:
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters('prematch-rmq.lsports.eu', 5672, "Customers", credentials))
        except ProbableAuthenticationError:
            logger.info(
                "ConnectionClosedByBroker: (403) ACCESS_REFUSED - Login was refused using authentication mechanism PLAIN")
            HTTPClient().fetch(get_request("".join([ENABLE_PREMATCH_URL, LOGIN_PREMATCH])))
            time.sleep(4)
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters('prematch-rmq.lsports.eu', 5672, "Customers", credentials))
        self.channel = self.connection.channel()
        self.channel.basic_consume(queue=f'_{LSPORTpackageIdPreMatch}_', on_message_callback=callback, auto_ack=True)

    def start(self):
        logger.info(' [*] Ready for messages.')
        try:
            self.channel.start_consuming()
        except Exception:
            self.channel.stop_consuming()


if __name__ == '__main__':
    rabbit_consumer = LsportRabbitPrematch()
    write_first_prematch_to_redis()
    rabbit_consumer.start()
