from copy import deepcopy

# For Highload Tests - generation of additional 50000 events:
EVENTS_QUANTITY = 50000
BETS_QUANTITY = 10
LOCATIONS_IN_SPORT = LEAGUES_IN_LOCATION = EVENTS_IN_LEAGUE = 20
MARKETS_IDS = (1, 2, 7, 13, 20, 30, 40, 50, 60, 70)
BET_STATUS_OPEN = 1
EVENT_STATUS_NOT_STARTED = 1
PROVIDER_ID = 8
DUMB_CONST = '333333'
LastUpdate = "2020-04-10T08:20:15.7109396Z"
StartDate = "2020-05-10T08:20:15"

PSEUDO_FIXTURE = {
    "Sport": {
        "Id": None,
        "Name": "Test_sport "
    },
    "Location": {
        "Id": None,
        "Name": "Test_country "
    },
    "League": {
        "Id": None,
        "Name": "Test Liga "
    },
    "StartDate": StartDate,
    "LastUpdate": LastUpdate,
    "Status": EVENT_STATUS_NOT_STARTED,
    "Participants": [
        {
            "Id": None,
            "Name": "Test Participant 1",
            "Position": "1"
        },
        {
            "Id": None,
            "Name": "Test Participant 2",
            "Position": "2"
        }
        ]
    }

PSEUDO_BET = {
    "Id": None,
    "Name": "Test bet",
    "Status": BET_STATUS_OPEN,
    "StartPrice": "1.0",
    "Price": "3.0",
    "LastUpdate": LastUpdate
    }

current_fixture_id = 1
currnet_bet_id = 1
sport_id = 1
location_id = 1
league_id = 1
participant_id = 1
locations_count = leagues_cont = events_count = 0

pseudo_provider = {
    "Id": PROVIDER_ID,
    "Name": "Test",
    "LastUpdate": LastUpdate,
    "ProviderFixtureId": DUMB_CONST,
    "ProviderMarketId": DUMB_CONST,
    "Bets": []
    }

for _ in range(BETS_QUANTITY):
    bet_ = deepcopy(PSEUDO_BET)
    pseudo_provider["Bets"].append(bet_)

pseudo_market = {
    "Id": None,
    "Name": "Test market",
    "Providers": []
    }

pseudo_event = {
    "FixtureId": None,
    'Fixture': PSEUDO_FIXTURE,
    'Markets': [],
    "Livescore": None
    }

for m_id in MARKETS_IDS:
    market_ = deepcopy(pseudo_market)
    market_["Id"] = m_id
    market_["Providers"].append(deepcopy(pseudo_provider))
    pseudo_event['Markets'].append(market_)


def fill_event(event):
    """Creating full event"""

    global current_fixture_id, currnet_bet_id, sport_id, location_id,\
        league_id, participant_id, locations_count, leagues_cont, events_count

    event["FixtureId"] = current_fixture_id
    current_fixture_id += 1
    markets = event['Markets']
    for market in markets:
        for bet in market["Providers"][0]["Bets"]:
            bet["Id"] = currnet_bet_id
            currnet_bet_id += 1
    fixt = event["Fixture"]
    fixt["Participants"][0]["Id"] = participant_id
    fixt["Participants"][1]["Id"] = participant_id + 1
    participant_id += 2
    if events_count >= EVENTS_IN_LEAGUE:
        league_id += 1
        leagues_cont += 1
        events_count = 0
    if leagues_cont >= LEAGUES_IN_LOCATION:
        location_id += 1
        locations_count += 1
        leagues_cont = 0
    if locations_count >= LOCATIONS_IN_SPORT:
        sport_id += 1
        locations_count = 0
    fixt["League"]["Id"] = league_id
    fixt["Location"]["Id"] = location_id
    fixt["Sport"]["Id"] = sport_id
    fixt["League"]["Name"] += str(league_id)
    fixt["Location"]["Name"] += str(location_id)
    fixt["Sport"]["Name"] += str(sport_id)
    events_count += 1


def generate_events():
    """For Highload Tests - generation of additional events"""

    events = []
    for _ in range(EVENTS_QUANTITY):
        event = deepcopy(pseudo_event)
        fill_event(event)
        events.append(event)
    return events

