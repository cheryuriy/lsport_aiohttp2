FROM python:3.7

ENV WAITFORIT_VERSION="v2.2.0"
COPY ./requirements.txt /
RUN curl -o /usr/local/bin/waitforit -sSL https://github.com/maxcnunes/waitforit/releases/download/$WAITFORIT_VERSION/waitforit-linux_amd64 \
  && chmod +x /usr/local/bin/waitforit \
  && pip install -r /requirements.txt \
  && PATH=/usr/local/bin:$PATH

COPY . /server/
