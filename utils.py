import asyncio
from decimal import Decimal
from typing import Dict, Optional, NoReturn, List, Union, Set

import aioredis
import redis
import ujson
import uvloop
from tornado.httpclient import HTTPRequest

from settings import REDIS, LSPORT_ONE_PROVIDER_ID

IDENTIFIERS_OF_LSPORT_OBJECTS = ("FixtureId", "Id", "Name", "Type", "Position")
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


class AsyncRedisPool:
    pool = dict()

    @classmethod
    async def get_instance(cls, db=REDIS["default_db"]):
        """
        Singletone async redis connection pool
        :param db: number of redis db
        :return: aioredis.Redis - connection
        """
        db = int(db) if not isinstance(db, int) else db
        if not REDIS:
            return None
        if not cls.pool.get(db):
            cls.pool[db] = await aioredis.create_redis_pool(
                address=(REDIS["host"], REDIS["port"]),
                password=REDIS["password"], db=db,
                maxsize=REDIS["max_pool_size"], loop=asyncio.get_event_loop()
            )
        return cls.pool[db]


class RedisPoll:
    pool = dict()

    @classmethod
    def get_instance(cls, db=REDIS["default_db"]):
        """
        Singletoned redis connection pool
        :param db: number of redis db
        :return: sync redis connection
        """
        db = int(db) if not isinstance(db, int) else db
        if not REDIS:
            return None
        if not cls.pool.get(db):
            cls.pool[db] = redis.ConnectionPool(
                host=REDIS['host'], port=REDIS['port'],
                password=REDIS['password'], db=db,
                max_connections=REDIS['max_pool_size']
            )
        return redis.Redis(connection_pool=cls.pool[db])


def get_request(url: str) -> HTTPRequest:
    """For requests by Tornado Client"""

    return HTTPRequest(
        url,
        method='GET',
        request_timeout=180,
        # headers=headers,
    )


def scan_for_identifiers(obj: Dict) -> Optional[str]:
    """Scanning for keys that can identify Lsport object."""

    item = obj[0]
    k = None
    for key in IDENTIFIERS_OF_LSPORT_OBJECTS:
        if item.get(key) is not None:
            k = key
            break
    if k:
        for i in obj:
            if i.get(k) is None:
                return None
        return k
    return None


def scan_for_multiple_identifiers(obj: Dict[str, Union[Dict, List, str]]) -> Optional[List[str]]:
    """Taking field names as multiple key identifier of Lsport object."""

    keys = set()
    for key, value in obj[0].items():
        if value is not None and not isinstance(value, dict) and not isinstance(value, list):
            keys.add(key)
    if not keys:
        return None
    for i in obj[1:]:
        not_found = keys - set(i.keys())
        for key in not_found:
            keys.discard(key)
    return sorted(list(keys))


def make_keys_and_dict_from_list(obj: List, key: str) -> (Optional[Set], Optional[Dict]):
    """
        Processing Lsport list by key:
        1) Getting identifiers of Lsport object by key -> obj_ids.
        2) Making dict: {identifier: object} -> obj_dict
    """

    obj_dict = dict()
    obj_ids = set()
    for i in obj:
        id_ = i.get(key)  # key = "Id"
        if id_ is not None:
            obj_ids.add(id_)
            obj_dict[id_] = i
        else:
            return None, None
    return obj_ids, obj_dict


def make_multiple_keys_and_dict_from_list(obj: List, keys: List[str]) -> (Optional[Set], Optional[Dict]):
    """
        Processing Lsport list by keys:
        1) Getting identifiers of Lsport object -> obj_ids.
        2) Making dict: {identifier: object} -> obj_dict
    """

    obj_dict = dict()
    obj_keys = set()
    for i in obj:
        lst = []
        for k in keys:
            value = i.get(k)
            if value is None:
                return None, None
            lst.append(value)
        keys_values = tuple(lst)
        if keys_values:
            obj_keys.add(keys_values)
            obj_dict[keys_values] = i
        else:
            return None, None
    return obj_keys, obj_dict


def update_obj(obj: Union[List, Dict], changes: Union[List, Dict]) -> NoReturn:
    """Update obj - object of Lsport by chanages."""

    if not changes:
        return
    if isinstance(obj, list):  # Updating lists obj, changes:
        if not isinstance(changes, list):
            raise Exception("changes not list!")
        if not obj:
            obj.extend(changes)
            return
        key = scan_for_identifiers(obj)
        if key:
            changes_keys, changes_dict = make_keys_and_dict_from_list(changes, key)
            if not changes_keys or not changes_dict:
                raise Exception(f"key:{key} not in changes list!")
            obj_keys, obj_dict = make_keys_and_dict_from_list(obj, key)
            ids = changes_keys - obj_keys  # - set
            for id_ in ids:
                obj.append(changes_dict[id_])
            ids = changes_keys.intersection(obj_keys)
            for id_ in ids:
                update_obj(obj_dict[id_], changes_dict[id_])
        else:
            keys = scan_for_multiple_identifiers(obj)
            if not keys:
                raise Exception(f"No any simple key in old events list!")
            changes_keys, changes_dict = make_multiple_keys_and_dict_from_list(changes, keys)
            if not changes_keys or not changes_dict:
                raise Exception(f"keys:{keys} not in changes list!")
            obj_keys, obj_dict = make_multiple_keys_and_dict_from_list(obj, keys)
            ids = changes_keys - obj_keys  # - set
            for id_ in ids:
                obj.append(changes_dict[id_])
            ids = changes_keys.intersection(obj_keys)
            for id_ in ids:
                update_obj(obj_dict[id_], changes_dict[id_])
    elif isinstance(obj, dict):  # Updating dicts obj, changes:
        if not isinstance(changes, dict):
            raise Exception("changes not dict!")
        for key, value in changes.items():
            obj_value = obj.get(key)
            if obj_value is None:
                obj[key] = value
            elif isinstance(obj_value, list) or isinstance(obj_value, dict):
                update_obj(obj_value, value)
            elif key not in ('Id', 'Name'):
                obj[key] = value


def put_in_2dicts(dict_, key1, key2, val):
    if key1 in dict_:
        dict_[key1][key2] = val
    else:
        dict_[key1] = {key2: val}


def set_bets_prices(markets, fixture_bets_prices, fixture_id):
    """1) Filtering markets by provider.
       2) Writing changes to fixture_bets_prices"""

    if markets:
        fixture_bets_prices[fixture_id] = {}
        for i in range(len(markets) - 1, -1, -1):
            filtered_provider = []
            for provider in markets[i]["Providers"]:
                if provider["Id"] == LSPORT_ONE_PROVIDER_ID:
                    filtered_provider.append(provider)
            if not filtered_provider:  # deletes market - filtering by 1 Provider
                markets.pop(i)
                continue
            markets[i]["Providers"] = filtered_provider
            market_id = str(markets[i]["Id"])
            fixture_bets_prices[fixture_id][market_id] = {}
            for provider in markets[i]["Providers"]:
                if provider.get("Bets"):
                    for bet in provider["Bets"]:
                        info = {"coef": bet["Price"], "status": bet["Status"]}
                        if bet.get("Settlement"):
                            info["result"] = bet["Settlement"]
                        fixture_bets_prices[fixture_id][market_id][str(bet["Id"])] = info


def process_bets_changes(fixture_bets_prices, live_or_prematch, logger):
    """
        1) Processing and writing bets_prices to redis
        2) Writing results( bet["Settlement"]) in fixture_bets_results
    """

    r = RedisPoll.get_instance()
    fixture_bets_results = {}
    for fixture_id, new_prices in fixture_bets_prices.items():
        old_prices = r.get(f"{live_or_prematch}_bets_prices_{fixture_id}")  # todo Pipeline is faster
        if old_prices:
            old_prices = ujson.loads(old_prices)
            for market_id, new_bets in new_prices.items():
                if market_id in old_prices:
                    old_market = old_prices[market_id]
                    for bet_id, bet_info in new_bets.items():
                        if bet_info.get("result"):
                            bet_result = bet_info["result"]
                            if bet_id in old_market:
                                if old_market[bet_id].get("result") != bet_result:
                                    put_in_2dicts(fixture_bets_results, (fixture_id, market_id), Decimal(bet_id), bet_result)
                            else:
                                logger.error(
                                    f"Got bet(mId={market_id}, bId={bet_id}) result, but bet not in {live_or_prematch}_bets_prices_{fixture_id} !")
                                put_in_2dicts(fixture_bets_results, (fixture_id, market_id), Decimal(bet_id), bet_result)
                else:
                    for bet_id, bet_info in new_bets.items():
                        if bet_info.get("result"):
                            put_in_2dicts(fixture_bets_results, (fixture_id, market_id), Decimal(bet_id), bet_info["result"])
                if market_id in old_prices:
                    old_prices[market_id].update(new_bets)  # fixture_id:{market_id: {betId: {betPrice, betStatus}}}
                else:
                    old_prices[market_id] = new_bets
            r.set(f"{live_or_prematch}_bets_prices_{fixture_id}", ujson.dumps(old_prices).encode('utf8'))
        else:
            for market_id, new_bets in new_prices.items():
                for bet_id, bet_info in new_bets.items():
                    if bet_info.get("result"):
                        put_in_2dicts(fixture_bets_results, (fixture_id, market_id), Decimal(bet_id), bet_info["result"])
            r.set(f"{live_or_prematch}_bets_prices_{fixture_id}", ujson.dumps(new_prices).encode('utf8'))
