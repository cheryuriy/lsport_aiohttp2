import asyncio

import uvloop
from aiohttp import web

from handlers import sports_locations, events_by_sport_and_location

# Setting urls to aiohttp handlers:
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
app = web.Application()
app.add_routes([
                web.get('/events-by-sport-location', events_by_sport_and_location),
                web.get('/sports-locations', sports_locations)])


if __name__ == '__main__':
    web.run_app(app)
