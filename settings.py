REDIS = {
    "host": "localhost",  # "redis", localhost
    "port": 6379,
    "password": "redis12345",
    "max_pool_size": 10,
    "default_db": 0
}

LSPORT_LANGUAGES = ["en"]
LSPORT_GOOD_EVENT_STATUSES_FOR_BET = (1, 2, 5, 9)  # 1 Not started yet, 2 In progress, 5 Postponed, 9 About to start
LSPORT_ONE_PROVIDER_ID = 8
LSPORT_BET_SETTLEMENT_TYPES = (-1, 1, 2, 3, 4, 5)
