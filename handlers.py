import calendar
import time
from logging import getLogger

import ujson
from aiohttp import web

from rabbit_prematch import LOGIN_PREMATCH
from utils import AsyncRedisPool

logger = getLogger(__name__)
LSPORT_PREMATCH_URL = 'http://prematch.lsports.eu/OddService/'
GET_EVENTS_URL = "".join([LSPORT_PREMATCH_URL, "GetEvents", LOGIN_PREMATCH])
FIRST_LANGUAGE = "en"
TIME_TO_CHECK_CHANGES = 6
kwargs_ = {"content_type": "text/plain", "charset": "charset=UTF-8"}  # For web.Response


async def sports_locations(request):
    """Handlers of aiohttp. Give all sports and locations."""

    redis = await AsyncRedisPool.get_instance()
    lang = request.query.get('lang', FIRST_LANGUAGE)
    result = await redis.get(f'prematch_sports_locations_{lang}')
    if result:
        return web.Response(body=result, **kwargs_)
    return web.Response(body=ujson.dumps([]).encode('utf8'), **kwargs_)


def is_time_right(ts: int, from_date: str, to_date: str) -> bool:
    """Checks if timestamp ts between timestamps from_date and to_date"""

    if from_date:
        from_date = int(from_date)
        if ts < from_date:
            return False
    if to_date:
        to_date = int(to_date)
        if ts > to_date:
            return False
    return True


async def events_by_sport_and_location(request):
    """Handlers of aiohttp. Give mathces by sport and location from redis."""

    sport_id = request.query.get('sportId', None)
    location_id = request.query.get('locationId', None)
    if not (sport_id and location_id):
        return web.Response(body=ujson.dumps({
            'status': -1,
            'result': None,
            'error_message': f"No {'sportId' if not sport_id else ''} {'locationId' if not location_id else ''} argument"
            }), content_type="application/json")

    from_date = request.query.get("fromDate", None)
    to_date = request.query.get("toDate", None)
    lang = request.query.get("lang", FIRST_LANGUAGE)
    redis = await AsyncRedisPool.get_instance()
    result = await redis.get(f'prematch_spo_{sport_id}_loc_{location_id}_lang_{lang}')
    if result and (from_date or to_date):
        events = ujson.loads(result)
        data = []
        for event in events:
            start_date = event['Fixture']["StartDate"]  # "2019-11-11T08:30:00"
            pattern = '%Y-%m-%dT%H:%M:%S'
            ts = int(calendar.timegm(time.strptime(start_date, pattern)))
            if is_time_right(ts, from_date, to_date):
                data.append(event)
        if data:
            return web.Response(body=ujson.dumps(data).encode('utf8'), **kwargs_)
        else:
            return web.Response(body=ujson.dumps([]).encode('utf8'), **kwargs_)
    else:
        if result:
            return web.Response(body=result, **kwargs_)
        else:
            return web.Response(body=ujson.dumps([]).encode('utf8'), **kwargs_)
